package br.com.riklenon.desafio.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.com.riklenon.desafio.IntegrationTest;
import br.com.riklenon.desafio.domain.Attendance;
import br.com.riklenon.desafio.domain.Sintoma;
import br.com.riklenon.desafio.repository.SintomaRepository;
import br.com.riklenon.desafio.service.criteria.SintomaCriteria;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link SintomaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class SintomaResourceIT {

    private static final String DEFAULT_DESCRICAO = "AAAAAAAAAA";
    private static final String UPDATED_DESCRICAO = "BBBBBBBBBB";

    private static final String DEFAULT_DETALHES = "AAAAAAAAAA";
    private static final String UPDATED_DETALHES = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/sintomas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private SintomaRepository sintomaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restSintomaMockMvc;

    private Sintoma sintoma;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sintoma createEntity(EntityManager em) {
        Sintoma sintoma = new Sintoma().descricao(DEFAULT_DESCRICAO).detalhes(DEFAULT_DETALHES);
        return sintoma;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Sintoma createUpdatedEntity(EntityManager em) {
        Sintoma sintoma = new Sintoma().descricao(UPDATED_DESCRICAO).detalhes(UPDATED_DETALHES);
        return sintoma;
    }

    @BeforeEach
    public void initTest() {
        sintoma = createEntity(em);
    }

    @Test
    @Transactional
    void createSintoma() throws Exception {
        int databaseSizeBeforeCreate = sintomaRepository.findAll().size();
        // Create the Sintoma
        restSintomaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sintoma)))
            .andExpect(status().isCreated());

        // Validate the Sintoma in the database
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeCreate + 1);
        Sintoma testSintoma = sintomaList.get(sintomaList.size() - 1);
        assertThat(testSintoma.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testSintoma.getDetalhes()).isEqualTo(DEFAULT_DETALHES);
    }

    @Test
    @Transactional
    void createSintomaWithExistingId() throws Exception {
        // Create the Sintoma with an existing ID
        sintoma.setId(1L);

        int databaseSizeBeforeCreate = sintomaRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restSintomaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sintoma)))
            .andExpect(status().isBadRequest());

        // Validate the Sintoma in the database
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllSintomas() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList
        restSintomaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sintoma.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)))
            .andExpect(jsonPath("$.[*].detalhes").value(hasItem(DEFAULT_DETALHES)));
    }

    @Test
    @Transactional
    void getSintoma() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get the sintoma
        restSintomaMockMvc
            .perform(get(ENTITY_API_URL_ID, sintoma.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(sintoma.getId().intValue()))
            .andExpect(jsonPath("$.descricao").value(DEFAULT_DESCRICAO))
            .andExpect(jsonPath("$.detalhes").value(DEFAULT_DETALHES));
    }

    @Test
    @Transactional
    void getSintomasByIdFiltering() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        Long id = sintoma.getId();

        defaultSintomaShouldBeFound("id.equals=" + id);
        defaultSintomaShouldNotBeFound("id.notEquals=" + id);

        defaultSintomaShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultSintomaShouldNotBeFound("id.greaterThan=" + id);

        defaultSintomaShouldBeFound("id.lessThanOrEqual=" + id);
        defaultSintomaShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllSintomasByDescricaoIsEqualToSomething() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where descricao equals to DEFAULT_DESCRICAO
        defaultSintomaShouldBeFound("descricao.equals=" + DEFAULT_DESCRICAO);

        // Get all the sintomaList where descricao equals to UPDATED_DESCRICAO
        defaultSintomaShouldNotBeFound("descricao.equals=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllSintomasByDescricaoIsNotEqualToSomething() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where descricao not equals to DEFAULT_DESCRICAO
        defaultSintomaShouldNotBeFound("descricao.notEquals=" + DEFAULT_DESCRICAO);

        // Get all the sintomaList where descricao not equals to UPDATED_DESCRICAO
        defaultSintomaShouldBeFound("descricao.notEquals=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllSintomasByDescricaoIsInShouldWork() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where descricao in DEFAULT_DESCRICAO or UPDATED_DESCRICAO
        defaultSintomaShouldBeFound("descricao.in=" + DEFAULT_DESCRICAO + "," + UPDATED_DESCRICAO);

        // Get all the sintomaList where descricao equals to UPDATED_DESCRICAO
        defaultSintomaShouldNotBeFound("descricao.in=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllSintomasByDescricaoIsNullOrNotNull() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where descricao is not null
        defaultSintomaShouldBeFound("descricao.specified=true");

        // Get all the sintomaList where descricao is null
        defaultSintomaShouldNotBeFound("descricao.specified=false");
    }

    @Test
    @Transactional
    void getAllSintomasByDescricaoContainsSomething() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where descricao contains DEFAULT_DESCRICAO
        defaultSintomaShouldBeFound("descricao.contains=" + DEFAULT_DESCRICAO);

        // Get all the sintomaList where descricao contains UPDATED_DESCRICAO
        defaultSintomaShouldNotBeFound("descricao.contains=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllSintomasByDescricaoNotContainsSomething() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where descricao does not contain DEFAULT_DESCRICAO
        defaultSintomaShouldNotBeFound("descricao.doesNotContain=" + DEFAULT_DESCRICAO);

        // Get all the sintomaList where descricao does not contain UPDATED_DESCRICAO
        defaultSintomaShouldBeFound("descricao.doesNotContain=" + UPDATED_DESCRICAO);
    }

    @Test
    @Transactional
    void getAllSintomasByDetalhesIsEqualToSomething() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where detalhes equals to DEFAULT_DETALHES
        defaultSintomaShouldBeFound("detalhes.equals=" + DEFAULT_DETALHES);

        // Get all the sintomaList where detalhes equals to UPDATED_DETALHES
        defaultSintomaShouldNotBeFound("detalhes.equals=" + UPDATED_DETALHES);
    }

    @Test
    @Transactional
    void getAllSintomasByDetalhesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where detalhes not equals to DEFAULT_DETALHES
        defaultSintomaShouldNotBeFound("detalhes.notEquals=" + DEFAULT_DETALHES);

        // Get all the sintomaList where detalhes not equals to UPDATED_DETALHES
        defaultSintomaShouldBeFound("detalhes.notEquals=" + UPDATED_DETALHES);
    }

    @Test
    @Transactional
    void getAllSintomasByDetalhesIsInShouldWork() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where detalhes in DEFAULT_DETALHES or UPDATED_DETALHES
        defaultSintomaShouldBeFound("detalhes.in=" + DEFAULT_DETALHES + "," + UPDATED_DETALHES);

        // Get all the sintomaList where detalhes equals to UPDATED_DETALHES
        defaultSintomaShouldNotBeFound("detalhes.in=" + UPDATED_DETALHES);
    }

    @Test
    @Transactional
    void getAllSintomasByDetalhesIsNullOrNotNull() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where detalhes is not null
        defaultSintomaShouldBeFound("detalhes.specified=true");

        // Get all the sintomaList where detalhes is null
        defaultSintomaShouldNotBeFound("detalhes.specified=false");
    }

    @Test
    @Transactional
    void getAllSintomasByDetalhesContainsSomething() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where detalhes contains DEFAULT_DETALHES
        defaultSintomaShouldBeFound("detalhes.contains=" + DEFAULT_DETALHES);

        // Get all the sintomaList where detalhes contains UPDATED_DETALHES
        defaultSintomaShouldNotBeFound("detalhes.contains=" + UPDATED_DETALHES);
    }

    @Test
    @Transactional
    void getAllSintomasByDetalhesNotContainsSomething() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        // Get all the sintomaList where detalhes does not contain DEFAULT_DETALHES
        defaultSintomaShouldNotBeFound("detalhes.doesNotContain=" + DEFAULT_DETALHES);

        // Get all the sintomaList where detalhes does not contain UPDATED_DETALHES
        defaultSintomaShouldBeFound("detalhes.doesNotContain=" + UPDATED_DETALHES);
    }

    @Test
    @Transactional
    void getAllSintomasByAttendanceIsEqualToSomething() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);
        Attendance attendance;
        if (TestUtil.findAll(em, Attendance.class).isEmpty()) {
            attendance = AttendanceResourceIT.createEntity(em);
            em.persist(attendance);
            em.flush();
        } else {
            attendance = TestUtil.findAll(em, Attendance.class).get(0);
        }
        em.persist(attendance);
        em.flush();
        sintoma.setAttendance(attendance);
        sintomaRepository.saveAndFlush(sintoma);
        Long attendanceId = attendance.getId();

        // Get all the sintomaList where attendance equals to attendanceId
        defaultSintomaShouldBeFound("attendanceId.equals=" + attendanceId);

        // Get all the sintomaList where attendance equals to (attendanceId + 1)
        defaultSintomaShouldNotBeFound("attendanceId.equals=" + (attendanceId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultSintomaShouldBeFound(String filter) throws Exception {
        restSintomaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sintoma.getId().intValue())))
            .andExpect(jsonPath("$.[*].descricao").value(hasItem(DEFAULT_DESCRICAO)))
            .andExpect(jsonPath("$.[*].detalhes").value(hasItem(DEFAULT_DETALHES)));

        // Check, that the count call also returns 1
        restSintomaMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultSintomaShouldNotBeFound(String filter) throws Exception {
        restSintomaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restSintomaMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingSintoma() throws Exception {
        // Get the sintoma
        restSintomaMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewSintoma() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        int databaseSizeBeforeUpdate = sintomaRepository.findAll().size();

        // Update the sintoma
        Sintoma updatedSintoma = sintomaRepository.findById(sintoma.getId()).get();
        // Disconnect from session so that the updates on updatedSintoma are not directly saved in db
        em.detach(updatedSintoma);
        updatedSintoma.descricao(UPDATED_DESCRICAO).detalhes(UPDATED_DETALHES);

        restSintomaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedSintoma.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedSintoma))
            )
            .andExpect(status().isOk());

        // Validate the Sintoma in the database
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeUpdate);
        Sintoma testSintoma = sintomaList.get(sintomaList.size() - 1);
        assertThat(testSintoma.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testSintoma.getDetalhes()).isEqualTo(UPDATED_DETALHES);
    }

    @Test
    @Transactional
    void putNonExistingSintoma() throws Exception {
        int databaseSizeBeforeUpdate = sintomaRepository.findAll().size();
        sintoma.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSintomaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, sintoma.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sintoma))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sintoma in the database
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchSintoma() throws Exception {
        int databaseSizeBeforeUpdate = sintomaRepository.findAll().size();
        sintoma.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSintomaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(sintoma))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sintoma in the database
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamSintoma() throws Exception {
        int databaseSizeBeforeUpdate = sintomaRepository.findAll().size();
        sintoma.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSintomaMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(sintoma)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Sintoma in the database
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateSintomaWithPatch() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        int databaseSizeBeforeUpdate = sintomaRepository.findAll().size();

        // Update the sintoma using partial update
        Sintoma partialUpdatedSintoma = new Sintoma();
        partialUpdatedSintoma.setId(sintoma.getId());

        partialUpdatedSintoma.detalhes(UPDATED_DETALHES);

        restSintomaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSintoma.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSintoma))
            )
            .andExpect(status().isOk());

        // Validate the Sintoma in the database
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeUpdate);
        Sintoma testSintoma = sintomaList.get(sintomaList.size() - 1);
        assertThat(testSintoma.getDescricao()).isEqualTo(DEFAULT_DESCRICAO);
        assertThat(testSintoma.getDetalhes()).isEqualTo(UPDATED_DETALHES);
    }

    @Test
    @Transactional
    void fullUpdateSintomaWithPatch() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        int databaseSizeBeforeUpdate = sintomaRepository.findAll().size();

        // Update the sintoma using partial update
        Sintoma partialUpdatedSintoma = new Sintoma();
        partialUpdatedSintoma.setId(sintoma.getId());

        partialUpdatedSintoma.descricao(UPDATED_DESCRICAO).detalhes(UPDATED_DETALHES);

        restSintomaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedSintoma.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedSintoma))
            )
            .andExpect(status().isOk());

        // Validate the Sintoma in the database
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeUpdate);
        Sintoma testSintoma = sintomaList.get(sintomaList.size() - 1);
        assertThat(testSintoma.getDescricao()).isEqualTo(UPDATED_DESCRICAO);
        assertThat(testSintoma.getDetalhes()).isEqualTo(UPDATED_DETALHES);
    }

    @Test
    @Transactional
    void patchNonExistingSintoma() throws Exception {
        int databaseSizeBeforeUpdate = sintomaRepository.findAll().size();
        sintoma.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSintomaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, sintoma.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(sintoma))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sintoma in the database
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchSintoma() throws Exception {
        int databaseSizeBeforeUpdate = sintomaRepository.findAll().size();
        sintoma.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSintomaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(sintoma))
            )
            .andExpect(status().isBadRequest());

        // Validate the Sintoma in the database
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamSintoma() throws Exception {
        int databaseSizeBeforeUpdate = sintomaRepository.findAll().size();
        sintoma.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restSintomaMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(sintoma)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Sintoma in the database
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteSintoma() throws Exception {
        // Initialize the database
        sintomaRepository.saveAndFlush(sintoma);

        int databaseSizeBeforeDelete = sintomaRepository.findAll().size();

        // Delete the sintoma
        restSintomaMockMvc
            .perform(delete(ENTITY_API_URL_ID, sintoma.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Sintoma> sintomaList = sintomaRepository.findAll();
        assertThat(sintomaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

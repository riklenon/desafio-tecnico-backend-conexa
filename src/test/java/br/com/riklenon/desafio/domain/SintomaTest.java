package br.com.riklenon.desafio.domain;

import static org.assertj.core.api.Assertions.assertThat;

import br.com.riklenon.desafio.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class SintomaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Sintoma.class);
        Sintoma sintoma1 = new Sintoma();
        sintoma1.setId(1L);
        Sintoma sintoma2 = new Sintoma();
        sintoma2.setId(sintoma1.getId());
        assertThat(sintoma1).isEqualTo(sintoma2);
        sintoma2.setId(2L);
        assertThat(sintoma1).isNotEqualTo(sintoma2);
        sintoma1.setId(null);
        assertThat(sintoma1).isNotEqualTo(sintoma2);
    }
}

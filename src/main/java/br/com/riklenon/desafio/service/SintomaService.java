package br.com.riklenon.desafio.service;

import br.com.riklenon.desafio.domain.Sintoma;
import br.com.riklenon.desafio.repository.SintomaRepository;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Sintoma}.
 */
@Service
@Transactional
public class SintomaService {

    private final Logger log = LoggerFactory.getLogger(SintomaService.class);

    private final SintomaRepository sintomaRepository;

    public SintomaService(SintomaRepository sintomaRepository) {
        this.sintomaRepository = sintomaRepository;
    }

    /**
     * Save a sintoma.
     *
     * @param sintoma the entity to save.
     * @return the persisted entity.
     */
    public Sintoma save(Sintoma sintoma) {
        log.debug("Request to save Sintoma : {}", sintoma);
        return sintomaRepository.save(sintoma);
    }

    /**
     * Partially update a sintoma.
     *
     * @param sintoma the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Sintoma> partialUpdate(Sintoma sintoma) {
        log.debug("Request to partially update Sintoma : {}", sintoma);

        return sintomaRepository
            .findById(sintoma.getId())
            .map(existingSintoma -> {
                if (sintoma.getDescricao() != null) {
                    existingSintoma.setDescricao(sintoma.getDescricao());
                }
                if (sintoma.getDetalhes() != null) {
                    existingSintoma.setDetalhes(sintoma.getDetalhes());
                }

                return existingSintoma;
            })
            .map(sintomaRepository::save);
    }

    /**
     * Get all the sintomas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Sintoma> findAll(Pageable pageable) {
        log.debug("Request to get all Sintomas");
        return sintomaRepository.findAll(pageable);
    }

    /**
     * Get one sintoma by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Sintoma> findOne(Long id) {
        log.debug("Request to get Sintoma : {}", id);
        return sintomaRepository.findById(id);
    }

    /**
     * Delete the sintoma by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Sintoma : {}", id);
        sintomaRepository.deleteById(id);
    }
}

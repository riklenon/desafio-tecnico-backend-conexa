package br.com.riklenon.desafio.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link br.com.riklenon.desafio.domain.Attendance} entity. This class is used
 * in {@link br.com.riklenon.desafio.web.rest.AttendanceResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /attendances?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class AttendanceCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter dataHora;

    private LongFilter sintomasId;

    private LongFilter idPacienteId;

    private Boolean distinct;

    public AttendanceCriteria() {}

    public AttendanceCriteria(AttendanceCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.dataHora = other.dataHora == null ? null : other.dataHora.copy();
        this.sintomasId = other.sintomasId == null ? null : other.sintomasId.copy();
        this.idPacienteId = other.idPacienteId == null ? null : other.idPacienteId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public AttendanceCriteria copy() {
        return new AttendanceCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getDataHora() {
        return dataHora;
    }

    public StringFilter dataHora() {
        if (dataHora == null) {
            dataHora = new StringFilter();
        }
        return dataHora;
    }

    public void setDataHora(StringFilter dataHora) {
        this.dataHora = dataHora;
    }

    public LongFilter getSintomasId() {
        return sintomasId;
    }

    public LongFilter sintomasId() {
        if (sintomasId == null) {
            sintomasId = new LongFilter();
        }
        return sintomasId;
    }

    public void setSintomasId(LongFilter sintomasId) {
        this.sintomasId = sintomasId;
    }

    public LongFilter getIdPacienteId() {
        return idPacienteId;
    }

    public LongFilter idPacienteId() {
        if (idPacienteId == null) {
            idPacienteId = new LongFilter();
        }
        return idPacienteId;
    }

    public void setIdPacienteId(LongFilter idPacienteId) {
        this.idPacienteId = idPacienteId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AttendanceCriteria that = (AttendanceCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(dataHora, that.dataHora) &&
            Objects.equals(sintomasId, that.sintomasId) &&
            Objects.equals(idPacienteId, that.idPacienteId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, dataHora, sintomasId, idPacienteId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AttendanceCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (dataHora != null ? "dataHora=" + dataHora + ", " : "") +
            (sintomasId != null ? "sintomasId=" + sintomasId + ", " : "") +
            (idPacienteId != null ? "idPacienteId=" + idPacienteId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}

package br.com.riklenon.desafio.service.dto;

import br.com.riklenon.desafio.domain.Patient;
import br.com.riklenon.desafio.domain.Sintoma;

import java.util.HashSet;
import java.util.Set;



public class AttendanceDTO {

    private Long id;

    private String dataHora;

    private Set<Sintoma> sintomas = new HashSet<>();

    private Patient idPaciente;

    public AttendanceDTO(Long id, String dataHora, Set<Sintoma> sintomas, Patient idPaciente){
        this.id = id;
        this.dataHora = dataHora;
        this.sintomas = sintomas;
        this.idPaciente = idPaciente;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataHora() {
        return this.dataHora;
    }

    public AttendanceDTO dataHora(String dataHora) {
        this.setDataHora(dataHora);
        return this;
    }

    public void setDataHora(String dataHora) {
        this.dataHora = dataHora;
    }

    public Set<Sintoma> getSintomas() {
        return this.sintomas;
    }

    public void setSintomas(Set<Sintoma> sintomas) {
        this.sintomas = sintomas;
    }

    public Patient getIdPaciente() {
        return this.idPaciente;
    }

    public void setIdPaciente(Patient patient) {
        this.idPaciente = patient;
    }

}

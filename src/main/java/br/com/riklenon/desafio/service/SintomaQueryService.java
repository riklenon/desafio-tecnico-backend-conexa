package br.com.riklenon.desafio.service;

import br.com.riklenon.desafio.domain.*; // for static metamodels
import br.com.riklenon.desafio.domain.Sintoma;
import br.com.riklenon.desafio.repository.SintomaRepository;
import br.com.riklenon.desafio.service.criteria.SintomaCriteria;
import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Sintoma} entities in the database.
 * The main input is a {@link SintomaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Sintoma} or a {@link Page} of {@link Sintoma} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class SintomaQueryService extends QueryService<Sintoma> {

    private final Logger log = LoggerFactory.getLogger(SintomaQueryService.class);

    private final SintomaRepository sintomaRepository;

    public SintomaQueryService(SintomaRepository sintomaRepository) {
        this.sintomaRepository = sintomaRepository;
    }

    /**
     * Return a {@link List} of {@link Sintoma} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Sintoma> findByCriteria(SintomaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Sintoma> specification = createSpecification(criteria);
        return sintomaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Sintoma} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Sintoma> findByCriteria(SintomaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Sintoma> specification = createSpecification(criteria);
        return sintomaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(SintomaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Sintoma> specification = createSpecification(criteria);
        return sintomaRepository.count(specification);
    }

    /**
     * Function to convert {@link SintomaCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Sintoma> createSpecification(SintomaCriteria criteria) {
        Specification<Sintoma> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Sintoma_.id));
            }
            if (criteria.getDescricao() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescricao(), Sintoma_.descricao));
            }
            if (criteria.getDetalhes() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDetalhes(), Sintoma_.detalhes));
            }
            if (criteria.getAttendanceId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getAttendanceId(),
                            root -> root.join(Sintoma_.attendance, JoinType.LEFT).get(Attendance_.id)
                        )
                    );
            }
        }
        return specification;
    }
}

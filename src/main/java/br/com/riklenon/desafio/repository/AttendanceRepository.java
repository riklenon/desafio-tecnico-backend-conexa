package br.com.riklenon.desafio.repository;

import br.com.riklenon.desafio.domain.Attendance;
import java.util.List;

import br.com.riklenon.desafio.service.dto.AttendanceDTO;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Attendance entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AttendanceRepository extends JpaRepository<Attendance, Long>, JpaSpecificationExecutor<Attendance> {

    @Query(value = "SELECT new br.com.riklenon.desafio.service.dto.AttendanceDTO(a.id, a.dataHora, a.sintomas, a.idPaciente) FROM Attendance a")
    List<AttendanceDTO> findAttendanceDTO();


}

package br.com.riklenon.desafio.repository;

import br.com.riklenon.desafio.domain.Sintoma;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Sintoma entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SintomaRepository extends JpaRepository<Sintoma, Long>, JpaSpecificationExecutor<Sintoma> {}

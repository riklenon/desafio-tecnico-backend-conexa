package br.com.riklenon.desafio.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Attendance.
 */
@Entity
@Table(name = "attendance")
public class Attendance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "data_hora")
    private String dataHora;

    @OneToMany(mappedBy = "attendance", fetch = FetchType.EAGER)
    private Set<Sintoma> sintomas = new HashSet<>();

    @ManyToOne
    @JoinColumn(nullable=false)
    @JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    private Patient idPaciente;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Attendance id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDataHora() {
        return this.dataHora;
    }

    public Attendance dataHora(String dataHora) {
        this.setDataHora(dataHora);
        return this;
    }

    public void setDataHora(String dataHora) {
        this.dataHora = dataHora;
    }

    public Set<Sintoma> getSintomas() {
        return this.sintomas;
    }

    public void setSintomas(Set<Sintoma> sintomas) {
        if (this.sintomas != null) {
            this.sintomas.forEach(i -> i.setAttendance(null));
        }
        if (sintomas != null) {
            sintomas.forEach(i -> i.setAttendance(this));
        }
        this.sintomas = sintomas;
    }

    public Attendance sintomas(Set<Sintoma> sintomas) {
        this.setSintomas(sintomas);
        return this;
    }

    public Attendance addSintomas(Sintoma sintoma) {
        this.sintomas.add(sintoma);
        sintoma.setAttendance(this);
        return this;
    }

    public Attendance removeSintomas(Sintoma sintoma) {
        this.sintomas.remove(sintoma);
        sintoma.setAttendance(null);
        return this;
    }

    public Patient getIdPaciente() {
        return this.idPaciente;
    }

    public void setIdPaciente(Patient patient) {
        this.idPaciente = patient;
    }

    public Attendance idPaciente(Patient patient) {
        this.setIdPaciente(patient);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Attendance)) {
            return false;
        }
        return id != null && id.equals(((Attendance) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Attendance{" +
            "id=" + getId() +
            ", dataHora='" + getDataHora() + "'" +
            "}";
    }
}

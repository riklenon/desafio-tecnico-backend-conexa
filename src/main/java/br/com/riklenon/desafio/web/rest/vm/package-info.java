/**
 * View Models used by Spring MVC REST controllers.
 */
package br.com.riklenon.desafio.web.rest.vm;

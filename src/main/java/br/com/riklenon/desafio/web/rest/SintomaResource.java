package br.com.riklenon.desafio.web.rest;

import br.com.riklenon.desafio.domain.Sintoma;
import br.com.riklenon.desafio.repository.SintomaRepository;
import br.com.riklenon.desafio.service.SintomaQueryService;
import br.com.riklenon.desafio.service.SintomaService;
import br.com.riklenon.desafio.service.criteria.SintomaCriteria;
import br.com.riklenon.desafio.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link br.com.riklenon.desafio.domain.Sintoma}.
 */
@RestController
@RequestMapping("/api")
public class SintomaResource {

    private final Logger log = LoggerFactory.getLogger(SintomaResource.class);

    private static final String ENTITY_NAME = "sintoma";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final SintomaService sintomaService;

    private final SintomaRepository sintomaRepository;

    private final SintomaQueryService sintomaQueryService;

    public SintomaResource(SintomaService sintomaService, SintomaRepository sintomaRepository, SintomaQueryService sintomaQueryService) {
        this.sintomaService = sintomaService;
        this.sintomaRepository = sintomaRepository;
        this.sintomaQueryService = sintomaQueryService;
    }

    /**
     * {@code POST  /sintomas} : Create a new sintoma.
     *
     * @param sintoma the sintoma to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new sintoma, or with status {@code 400 (Bad Request)} if the sintoma has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sintomas")
    public ResponseEntity<Sintoma> createSintoma(@RequestBody Sintoma sintoma) throws URISyntaxException {
        log.debug("REST request to save Sintoma : {}", sintoma);
        if (sintoma.getId() != null) {
            throw new BadRequestAlertException("A new sintoma cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Sintoma result = sintomaService.save(sintoma);
        return ResponseEntity
            .created(new URI("/api/sintomas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /sintomas/:id} : Updates an existing sintoma.
     *
     * @param id the id of the sintoma to save.
     * @param sintoma the sintoma to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sintoma,
     * or with status {@code 400 (Bad Request)} if the sintoma is not valid,
     * or with status {@code 500 (Internal Server Error)} if the sintoma couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sintomas/{id}")
    public ResponseEntity<Sintoma> updateSintoma(@PathVariable(value = "id", required = false) final Long id, @RequestBody Sintoma sintoma)
        throws URISyntaxException {
        log.debug("REST request to update Sintoma : {}, {}", id, sintoma);
        if (sintoma.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sintoma.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sintomaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Sintoma result = sintomaService.save(sintoma);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sintoma.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /sintomas/:id} : Partial updates given fields of an existing sintoma, field will ignore if it is null
     *
     * @param id the id of the sintoma to save.
     * @param sintoma the sintoma to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated sintoma,
     * or with status {@code 400 (Bad Request)} if the sintoma is not valid,
     * or with status {@code 404 (Not Found)} if the sintoma is not found,
     * or with status {@code 500 (Internal Server Error)} if the sintoma couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/sintomas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<Sintoma> partialUpdateSintoma(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Sintoma sintoma
    ) throws URISyntaxException {
        log.debug("REST request to partial update Sintoma partially : {}, {}", id, sintoma);
        if (sintoma.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, sintoma.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!sintomaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Sintoma> result = sintomaService.partialUpdate(sintoma);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, sintoma.getId().toString())
        );
    }

    /**
     * {@code GET  /sintomas} : get all the sintomas.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of sintomas in body.
     */
    @GetMapping("/sintomas")
    public ResponseEntity<List<Sintoma>> getAllSintomas(
        SintomaCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get Sintomas by criteria: {}", criteria);
        Page<Sintoma> page = sintomaQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /sintomas/count} : count all the sintomas.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/sintomas/count")
    public ResponseEntity<Long> countSintomas(SintomaCriteria criteria) {
        log.debug("REST request to count Sintomas by criteria: {}", criteria);
        return ResponseEntity.ok().body(sintomaQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /sintomas/:id} : get the "id" sintoma.
     *
     * @param id the id of the sintoma to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the sintoma, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sintomas/{id}")
    public ResponseEntity<Sintoma> getSintoma(@PathVariable Long id) {
        log.debug("REST request to get Sintoma : {}", id);
        Optional<Sintoma> sintoma = sintomaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(sintoma);
    }

    /**
     * {@code DELETE  /sintomas/:id} : delete the "id" sintoma.
     *
     * @param id the id of the sintoma to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sintomas/{id}")
    public ResponseEntity<Void> deleteSintoma(@PathVariable Long id) {
        log.debug("REST request to delete Sintoma : {}", id);
        sintomaService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}

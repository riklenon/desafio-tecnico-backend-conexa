package br.com.riklenon.desafio.web.rest.vm;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginDesafioVM {

    @NotNull
    @Size(min = 1, max = 50)
    String email;

    @NotNull
    @Size(min = 1, max = 50)
    String senha;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}

package br.com.riklenon.desafio.web.rest.v1;

import br.com.riklenon.desafio.domain.Attendance;
import br.com.riklenon.desafio.repository.AttendanceRepository;
import br.com.riklenon.desafio.service.AttendanceQueryService;
import br.com.riklenon.desafio.service.AttendanceService;
import br.com.riklenon.desafio.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1")
public class AttendanceResourceV1 {

    private final Logger log = LoggerFactory.getLogger(AttendanceResourceV1.class);

    private static final String ENTITY_NAME = "attendance";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AttendanceService attendanceService;

    private final AttendanceRepository attendanceRepository;

    private final AttendanceQueryService attendanceQueryService;

    public AttendanceResourceV1(
        AttendanceService attendanceService,
        AttendanceRepository attendanceRepository,
        AttendanceQueryService attendanceQueryService
    ) {
        this.attendanceService = attendanceService;
        this.attendanceRepository = attendanceRepository;
        this.attendanceQueryService = attendanceQueryService;
    }

    @GetMapping("/attendances")
    public List<Attendance> getAllAttendances() {
        log.debug("REST request to get Attendances");
        return attendanceRepository.findAll();
    }

    @PostMapping("/attendances")
    public ResponseEntity<Attendance> createAttendance(@RequestBody Attendance attendance) throws URISyntaxException {
        log.debug("REST request to save Attendance : {}", attendance);
        if (attendance.getId() != null) {
            throw new BadRequestAlertException("A new attendance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(System.currentTimeMillis()));
        calendar.add(Calendar.HOUR_OF_DAY, 1);
        try {
            Date date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm").parse(attendance.getDataHora());
            if (!date1.after(calendar.getTime())) {
                throw new BadRequestAlertException("DataHora ", ENTITY_NAME, " data recente invalida");
            }
        } catch (ParseException e) {
            System.err.println("Formato incorreto");
            throw new BadRequestAlertException("DataHora ", ENTITY_NAME, " invalido, formato valido 2000-01-01T01:01");
        }
        if (attendance.getDataHora().equals(null) ) {
            throw new BadRequestAlertException("DataHora ", ENTITY_NAME, " não nulo");
        }
        if (attendance.getDataHora().isBlank()) {
            throw new BadRequestAlertException("DataHora ", ENTITY_NAME, " vazio");
        }
        Attendance result = attendanceService.save(attendance);
        return ResponseEntity
            .created(new URI("/api/attendances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

}
